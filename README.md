# Baldur's Gate: Enhanced Edition - Audio Configurations
> bg-ee-audio

## Description
This repository contains some of the `lang` directory folders. Under the `customs` directory, configurations can be found of the game that will allow the text to be in **English**, but *the audio to be in other languages*.

For example, I have have kept the text **English** but the audio **Russian**. Although the translations are obviously not mapping directly to each other, I have done this primarily for the following two reasons:
- I like how the Russian language sounds, but still want to understand the game and read all the texts in English.
- I am currently learning Russian as well, thus this may prove somewhat useful for my vocabulary.

## Configurations
> Note that it is possible to replace either the `text` language or the `audio` language. By default, I replace the `text` language.

- [**English** *Text* & **Russian** *Audio*](https://gitlab.com/dainank/bg-ee-audio/-/releases/en_US-text%2Fru_RU-audio) - Replacing `en_US` (English)

## Installation
> Make sure that the game is **not** running before doing the following steps.

1. Select a configuration from [configurations list above](https://gitlab.com/dainank/bg-ee-audio#configurations).
2. Download and unzip the configuration.
3. Copy that configuration and paste/replace the directory in your local game files. Mine are located here: `C:\Program Files (x86)\Steam\steamapps\common\Baldur's Gate Enhanced Edition\lang`
    - see the README under the directory on this repo to see what the configuration is exactly altering. For example, this is the README for an [**English** *Text* & **Russian** *Audio*](./customs/en_US/README.md) configuration.

## Usage
> Make sure to have **completed** the installation step above before reading this section.

You can now access your modified audio/strings setup by **going to the game settings in-game, and selecting the base folder you replaced** (see example below). 

For example:
- If you replaced `en_US` you would then want to select English in the in-game menu. 
- If you replaced `ru_RU`, you would then want to select Russian in the in-game menu.

## Support
If you have accidently broken your game, you can revert back to a stable build through Steam's `"verify integrity of game files"` button: ![Example showing where to restore stable build.](./assets/image.png)

For further questions, you can contact me through:
- *Discord* (recommended): [`dainank`](https://discord.com/users/996158045388292296)

## Roadmap
Currently, I have only added my own configurations.

## Contributing
If you have a configuration that you would like to add to this repository, please let me know.

## `lang` Directory Details
> Miscellaneous information regarding the files within the `lang` directory of the game files.

- `dialog.tlk` - Localization strings for respective language.
    - `dialogF.tlk` - Additional female related localizations.

- `./sounds/*` - Player sounds only, for respective language.
- `./fonts/*` - Fonts for respective language.
- `./data/*` - Most game sounds except for player sounds (*see above*).

With this information, you can copy only what you desire when setting up configurations. In my case, since I only wanted audio in Russian but text remaining in English, I did not need the `./fonts/*` directory with `ru_RU`.
