# Configurations List

> Note that it is possible to replace either the `text` language or the `audio` language. By default, I replace the `text` language.

- [**English** *Text* & **Russian** *Audio*](./customs/en_US/README.md) - Replacing `en_US` (English)