# en_US Text - ru_RU Audio

- Text: `en_US` (**English**)
- Audio: `ru_RU` (**Russian**)
- Movies: `en_US` (**English**)

## TODO
- [x] ~~Possibly merge other *slavic* audio files to further populate the lacking Russian audio content:~~ -> Cancelled! Reasoning: Full-audio available, not reason for this.
    - [x] ~~`uk_UA` (has full audio)~~ -> make unique custom
    - [x] ~~`pl_PL` (has full audio)~~ -> make unique custom
    - [x] ~~`cz_CZ`~~
    - [x] ~~`hu_HU`~~
